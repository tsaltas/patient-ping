package tsaltas;

import java.util.Objects;

import org.joda.time.DateTime;

public class Patient {
  /**
   * Enum to contain M/F sex
   */
  public enum Sex {
    MALE,
    FEMALE;

    public static Sex stringToSex(final String value) {
      switch (value) {
        case "F":
          return Sex.FEMALE;
        case "M":
          return Sex.MALE;
        default:
          throw new IllegalArgumentException("Sex must be F or M");
      }
    }
  }

  private final String firstName;
  private final String lastName;
  private final DateTime birthDate;
  private final Sex sex;

  public Patient(
    final String firstName,
    final String lastName,
    final DateTime birthDate,
    final Sex sex
  ) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthDate = birthDate;
    this.sex = sex;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public DateTime getBirthDate() {
    return birthDate;
  }

  public Sex getSex() {
    return sex;
  }

  @Override
  public boolean equals(final Object obj) {
    if (!(obj instanceof Patient)) {
      return false;
    }
    final Patient that = (Patient) obj;

    return (
      firstName.equals(that.firstName) &&
        lastName.equals(that.lastName) &&
        birthDate.equals(that.birthDate) &&
        sex.equals(that.sex)
    );
  }

  @Override
  public int hashCode() {
    return Objects.hash(firstName, lastName, birthDate, sex);
  }

  @Override
  public String toString() {
    return String.format("%s %s, %s, %s", firstName, lastName, birthDate, sex);
  }
}
