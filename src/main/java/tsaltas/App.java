package tsaltas;

import java.io.PrintStream;
import java.util.List;

public class App {
  /**
   * Main method opens and parses a CSV file specified as first command line arg,
   * calculates patient metrics, and prints results to System.out
   */
  public static void main(String[] args) {
    final List<Patient> patients;
    try {
      // Parse patient data from CSV (filename passed from command line)
      patients = CsvParser.readPeopleWithCsvListReader(args[0]);

      // Calculate results and print to System.out
      printToSystemOut(
        String.format("Female patients: %s", ProcessPatientData.countWomen(patients))
      );
      printToSystemOut(
        String.format("Male patients: %s", ProcessPatientData.countMen(patients))
      );
      printToSystemOut(
        String.format("Patients per decade: %s", ProcessPatientData.countDecades(patients))
      );
      printToSystemOut(
        String.format("Most common female name: %s", ProcessPatientData.mostCommonFemaleName(patients))
      );
      printToSystemOut(
        String.format("Most common male name: %s", ProcessPatientData.mostCommonMaleName(patients))
      );
      printToSystemOut(
        String.format("Female patients sorted: %s", ProcessPatientData.femalesSorted(patients))
      );
      printToSystemOut(
        String.format("Male patients sorted: %s", ProcessPatientData.malesSorted(patients))
      );
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Helper to print to any {@link PrintStream}
   */
  private static void printResult(final String result, final PrintStream outputStream) {
    outputStream.println(result);
  }

  /**
   * Helper to print to {@link System} out
   */
  private static void printToSystemOut(final String result) {
    printResult(result, System.out);
  }
}
