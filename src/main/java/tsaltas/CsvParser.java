package tsaltas;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.supercsv.io.CsvListReader;
import org.supercsv.io.ICsvListReader;
import org.supercsv.prefs.CsvPreference;

public class CsvParser {
  /**
   * Reads in data from CSV.
   * Returns a list of Patients.
   */
  public static List<Patient> readPeopleWithCsvListReader(final String filename) throws Exception {
    ICsvListReader listReader = null;
    List<Patient> patientList = new ArrayList<>(); // initialize output

    try {
      listReader = new CsvListReader(new FileReader(filename), CsvPreference.STANDARD_PREFERENCE);

      listReader.getHeader(true); // skip the header (can't be used with CsvListReader)

      List<String> rowList;
      while ((rowList = listReader.read()) != null) {
        patientList.add(parsePatientRow(rowList));
      }

    } finally {
      if (listReader != null) {
        listReader.close();
      }
    }
    return patientList;
  }

  /**
   * Helper to parse single row from strings into Patient POJO
   */
  private static Patient parsePatientRow(final List<String> row) {
    // Note: tried to parse dates using SuperCSV CellProcessor but was buggy on dates like 2/5/00 (mapped to 2001)
    DateTimeFormatter fmt = DateTimeFormat.forPattern("MM/dd/yy");

    return  new Patient(
      row.get(0),
      row.get(1),
      fmt.parseDateTime(row.get(2)),
      Patient.Sex.stringToSex(row.get(3))
    );
  }
}



