package tsaltas;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ProcessPatientData {
	public static long countWomen(List<Patient> patients) {
		return patients
			.stream()
			.filter(p -> p.getSex().equals(Patient.Sex.FEMALE))
			.count();
	}

	public static long countMen(List<Patient> patients) {
		return patients
			.stream()
			.filter(p -> p.getSex().equals(Patient.Sex.MALE))
			.count();
	}

	// TODO: WIP
	public static Map<String, Integer> countDecades(List<Patient> patients) {
		return new HashMap<>();
	}

	// TODO: WIP
	public static String mostCommonMaleName(List<Patient> patients) {
		patients
			.stream()
			.filter(p -> p.getSex().equals(Patient.Sex.MALE))
			.collect(Collectors.groupingBy(Patient::getFirstName, Collectors.counting()));

		return null;
	}

	// TODO: WIP
	public static String mostCommonFemaleName(List<Patient> patients) {
		return null;
	}

	public static List<Patient> malesSorted(List<Patient> patients) {
		return patients
			.stream()
			.filter(p -> p.getSex().equals(Patient.Sex.MALE))
			.sorted((p1, p2) -> p1.getLastName().compareTo(p2.getLastName()))
			.collect(Collectors.toList());
	}

	public static List<Object> femalesSorted(List<Patient> patients) {
		return patients
			.stream()
			.filter(p -> p.getSex().equals(Patient.Sex.FEMALE))
			.sorted((p1, p2) -> p1.getLastName().compareTo(p2.getLastName()))
			.collect(Collectors.toList());
	}
}
