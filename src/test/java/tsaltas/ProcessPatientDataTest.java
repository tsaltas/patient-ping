package tsaltas;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.Ignore;
import org.junit.Test;

public class ProcessPatientDataTest {
  private static final List<Patient> PATIENT_LIST = Arrays.asList(
    new Patient("Nick", "Roberts", DateTime.parse("2016-01-01"), Patient.Sex.MALE),
    new Patient("Nick", "At Nite", DateTime.parse("2016-01-02"), Patient.Sex.MALE),
    new Patient("Sam", "Adams", DateTime.parse("2016-01-03"), Patient.Sex.MALE),
    new Patient("Sam", "Jones", DateTime.parse("2016-01-04"), Patient.Sex.MALE),
    new Patient("George", "Watson", DateTime.parse("2016-01-05"), Patient.Sex.MALE),
    new Patient("Nicole", "Roberts", DateTime.parse("2016-01-06"), Patient.Sex.FEMALE),
    new Patient("Samantha", "Adams", DateTime.parse("2016-01-07"), Patient.Sex.FEMALE),
    new Patient("Samantha", "Jones", DateTime.parse("2016-01-08"), Patient.Sex.FEMALE)
  );

  private static final List<Patient> SORTED_MALES = Arrays.asList(
    new Patient("Sam", "Adams", DateTime.parse("2016-01-03"), Patient.Sex.MALE),
    new Patient("Nick", "At Nite", DateTime.parse("2016-01-02"), Patient.Sex.MALE),
    new Patient("Sam", "Jones", DateTime.parse("2016-01-04"), Patient.Sex.MALE),
    new Patient("Nick", "Roberts", DateTime.parse("2016-01-01"), Patient.Sex.MALE),
    new Patient("George", "Watson", DateTime.parse("2016-01-05"), Patient.Sex.MALE)
  );

  private static final List<Patient> SORTED_FEMALES = Arrays.asList(
    new Patient("Samantha", "Adams", DateTime.parse("2016-01-07"), Patient.Sex.FEMALE),
    new Patient("Samantha", "Jones", DateTime.parse("2016-01-08"), Patient.Sex.FEMALE),
    new Patient("Nicole", "Roberts", DateTime.parse("2016-01-06"), Patient.Sex.FEMALE)
  );

  @Ignore // TODO:  Implement this
  @Test
  public void testMostCommonMaleName() {
    assertTrue(
      ProcessPatientData.mostCommonMaleName(PATIENT_LIST).equals("Nick") ||
      ProcessPatientData.mostCommonMaleName(PATIENT_LIST).equals("Sam")
    );
  }

  @Ignore // TODO:  Implement this
  @Test
  public void testMostCommonFemaleName() {
    assertEquals(ProcessPatientData.mostCommonMaleName(PATIENT_LIST), "Samantha");
  }

  @Test
  public void testCountMales() {
    assertEquals(ProcessPatientData.countMen(PATIENT_LIST), 5);
  }

  @Test
  public void testCountFemales() {
    assertEquals(ProcessPatientData.countWomen(PATIENT_LIST), 3);
  }

  @Test
  public void testSortMales() {
    assertEquals(ProcessPatientData.malesSorted(PATIENT_LIST), SORTED_MALES);
  }

  @Test
  public void testSortFemales() {
    assertEquals(ProcessPatientData.femalesSorted(PATIENT_LIST), SORTED_FEMALES);
  }

}
